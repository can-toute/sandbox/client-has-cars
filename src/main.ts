import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
      stopAtFirstError: false,
      transformOptions: {
        enableImplicitConversion: true,
        enableCircularCheck: true,
      },
    }),
  );

  const swaggerOptions = new DocumentBuilder()
    .setTitle('Sandbox Client has Cars')
    .setDescription('Testing nested models')
    .setVersion(process.env.npm_package_version)
    .build();

  const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('swagger', app, swaggerDocument);

  const PORT = 3000;

  await app.listen(PORT);

  console.log(
    `Swagger loaded: try out the api here http://localhost:${PORT}/swagger/`,
  );
}
bootstrap();
