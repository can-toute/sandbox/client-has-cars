import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsOptional,
  IsArray,
  IsString,
  IsInt,
  IsNumber,
  IsEnum,
  IsObject,
  IsJSON,
  ValidateNested,
} from 'class-validator';

import { Car } from 'src/cars/entities/car.entity';

export class Client {
  @ApiProperty()
  @Type(() => Number)
  id: number;

  @ApiProperty()
  @Type(() => String)
  name: string;

  // Broken !!!
  @ApiProperty({
    type: () => Car,
    isArray: true,
  })
  @Type(() => Car)
  @IsArray()
  // @IsObject({ each: true })
  cars: Car[];

  // @ApiProperty({
  //   type: () => Client,
  //   isArray: true,
  // })
  // @Type(() => Client)
  // @IsArray()
  // // @IsObject()
  // clients: Client[];
}
