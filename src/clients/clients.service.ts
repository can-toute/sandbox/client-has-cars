import { Injectable } from '@nestjs/common';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import { Client } from './entities/client.entity';
import { Car } from 'src/cars/entities/car.entity';

@Injectable()
export class ClientsService {
  // create(createClientDto: CreateClientDto) {
  //   return 'This action adds a new client';
  // }

  async findAll(): Promise<Client[]> {
    return new Promise<Client[]>((resolve) => resolve(<Client[]>[]));
  }

  // findOne(id: number) {
  //   return `This action returns a #${id} client`;
  // }

  // update(id: number, updateClientDto: UpdateClientDto) {
  //   return `This action updates a #${id} client`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} client`;
  // }
}
