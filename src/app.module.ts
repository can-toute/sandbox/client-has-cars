import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule } from './clients/clients.module';
import { CarsModule } from './cars/cars.module';

@Module({
  imports: [ClientsModule, CarsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
