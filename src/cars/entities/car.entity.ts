import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray } from 'class-validator';
import { Client } from 'src/clients/entities/client.entity';

export class Car {
  @ApiProperty({
    type: Number,
  })
  @Type(() => Number)
  id: number;

  @ApiProperty({
    type: String,
  })
  @Type(() => String)
  brand: string;

  @ApiProperty({
    type: Number,
  })
  @Type(() => Number)
  modelYear: number;

  // @ApiProperty()
  // @Type(() => Number)
  // clientId: number;

  // @ApiProperty({
  //   type: () => Client,
  // })
  // @Type(() => Client)
  // client: Client;

  // @ApiProperty({
  //   type: () => Client,
  //   isArray: true,
  // })
  // @Type(() => Client)
  // @IsArray()
  // clients: Client[];
}

// export class CarDto implements Car{

// }
