import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ApiOkResponse, ApiResponseProperty } from '@nestjs/swagger';
import { CarsService } from './cars.service';
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';
import { Car } from './entities/car.entity';

@Controller('cars')
export class CarsController {
  constructor(private readonly carsService: CarsService) {}

  // @Post()
  // create(@Body() createCarDto: CreateCarDto) {
  //   return this.carsService.create(createCarDto);
  // }

  @ApiOkResponse({
    type: () => Car,
    isArray: true,
  })
  @Get()
  async findAll(): Promise<Car[]> {
    return this.carsService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.carsService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateCarDto: UpdateCarDto) {
  //   return this.carsService.update(+id, updateCarDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.carsService.remove(+id);
  // }
}
